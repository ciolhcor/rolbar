<?php

class callerTest extends PHPUnit_Framework_TestCase
{
  public static function calledStandard() {
   return caller();
  }
  public static function calledStandardParam($param) {
    return caller($param);
  }
  public static function callerStandardStatic(&$line, &$function) {
    $function = __FUNCTION__;
    $line = __LINE__ + 1;
    return self::calledStandard();
  }
  public static function callerStandardStaticParam($param, &$line, &$function) {
    $function = __FUNCTION__;
    $line = __LINE__ + 1;
    return self::calledStandardParam($param);
  }

  public function testStandard() {
    $array = self::calledStandard();
    $this->assertEquals(__LINE__ - 1, $array['line'], 'caller() test line');
    $this->assertEquals(__FILE__, $array['file'], 'caller() test file');
    $this->assertEquals(__FUNCTION__, $array['function'], 'caller() test function');
    $this->assertEquals('->', $array['type'], 'caller() test type');
    $this->assertEquals(__CLASS__, $array['class'], 'caller() test class');
    $this->assertEquals($this, $array['object'], 'caller() test object');
  }

  public function testStandardStatic() {
    $array = self::callerStandardStatic($line, $function);
    $this->assertEquals($line, $array['line'], 'caller() test line');
    $this->assertEquals(__FILE__, $array['file'], 'caller() test file');
    $this->assertEquals($function, $array['function'], 'caller() test function');
    $this->assertEquals('::', $array['type'], 'caller() test type');
    $this->assertEquals(__CLASS__, $array['class'], 'caller() test class');
    $this->assertEquals(null, $array['object'], 'caller() test object');
  }

  public function testObject() {
    $obj = self::calledStandardParam(CALLER_OBJECT);
    $this->assertEquals($this, $obj, 'caller() test object');
  }
  public function testClass() {
    $class = self::calledStandardParam(CALLER_CLASS);
    $this->assertEquals(__CLASS__, $class, 'caller() test class');
  }
  public function testMethod() {
    $method = self::calledStandardParam(CALLER_METHOD);
    $this->assertEquals(__METHOD__, $method, 'caller() test method');
  }
  public function testFunction() {
    $function = self::calledStandardParam(CALLER_FUNCTION);
    $this->assertEquals(__FUNCTION__, $function, 'caller() test function');
  }


  /**
   * @expectedException OutOfBoundsException
   */
  public function testObjectFromFunction() {
    $obj = callerTest_callerReflexionFunction(CALLER_OBJECT);
  }
  /**
   * @expectedException OutOfBoundsException
   */
  public function testClassFromFunction() {
    $class = callerTest_callerReflexionFunction(CALLER_CLASS);
  }
  /**
   * @expectedException OutOfBoundsException
   */
  public function testMethodFromFunction() {
    $method = callerTest_callerReflexionFunction(CALLER_METHOD);
  }
  public function testFunctionFromFunction() {
    $function = callerTest_callerReflexionFunction(CALLER_FUNCTION, $funcname);
    $this->assertEquals($funcname, $function, 'caller() test function');
  }

  /**
   * @expectedException OutOfBoundsException
   */
  public function testObjectFromStatic() {
    $obj = self::callerStandardStaticParam(CALLER_OBJECT, $line, $function);
  }
  public function testClassFromStatic() {
    $class = self::callerStandardStaticParam(CALLER_CLASS, $line, $function);
    $this->assertEquals(__CLASS__, $class, 'caller() test class');
  }
  public function testMethodFromStatic() {
    $method = self::callerStandardStaticParam(CALLER_METHOD, $line, $function);
    $this->assertEquals(__CLASS__.'::'.$function, $method, 'caller() test method');
  }
  public function testFunctionFromStatic() {
    $function = self::callerStandardStaticParam(CALLER_FUNCTION, $line, $funcname);
    $this->assertEquals($funcname, $function, 'caller() test function');
  }


  public function testReflexionObject() {
    $obj = self::calledStandardParam(CALLER_OBJECT | CALLER_REFLEXION);
    $this->assertEquals(new ReflectionObject($this), $obj, 'caller() test reflexion object');
  }
  public function testReflexionClass() {
    $class = self::calledStandardParam(CALLER_CLASS | CALLER_REFLEXION);
    $this->assertEquals(new ReflectionClass(__CLASS__), $class, 'caller() test reflexion class');
  }
  public function testReflexionMethod() {
    $method = self::calledStandardParam(CALLER_METHOD | CALLER_REFLEXION);
    $this->assertEquals(new ReflectionMethod(__METHOD__), $method, 'caller() test reflexion method');
  }
  /**
   * @expectedException OutOfBoundsException
   */
  public function testReflexionFunction() {
    $function = self::calledStandardParam(CALLER_FUNCTION | CALLER_REFLEXION);
  }

  /**
   * @expectedException OutOfBoundsException
   */
  public function testReflexionObjectFromFunction() {
    $obj = callerTest_callerReflexionFunction(CALLER_OBJECT | CALLER_REFLEXION);
  }
  /**
   * @expectedException OutOfBoundsException
   */
  public function testReflexionClassFromFunction() {
    $class = callerTest_callerReflexionFunction(CALLER_CLASS | CALLER_REFLEXION);
  }
  /**
   * @expectedException OutOfBoundsException
   */
  public function testReflexionMethodFromFunction() {
    $method = callerTest_callerReflexionFunction(CALLER_METHOD | CALLER_REFLEXION);
  }
  public function testReflexionFunctionFromFunction() {
    $function = callerTest_callerReflexionFunction(CALLER_FUNCTION | CALLER_REFLEXION, $funcname);
    $this->assertEquals(new ReflectionFunction($funcname), $function, 'caller() test reflexion function');
  }

  /**
   * @expectedException OutOfBoundsException
   */
  public function testReflexionObjectFromStatic() {
    $obj = self::callerStandardStaticParam(CALLER_OBJECT | CALLER_REFLEXION, $line, $function);
  }
  public function testReflexionClassFromStatic() {
    $class = self::callerStandardStaticParam(CALLER_CLASS | CALLER_REFLEXION, $line, $function);
    $this->assertEquals(new ReflectionClass(__CLASS__), $class, 'caller() test reflexion class');
  }
  public function testReflexionMethodFromStatic() {
    $method = self::callerStandardStaticParam(CALLER_METHOD | CALLER_REFLEXION, $line, $function);
    $this->assertEquals(new ReflectionMethod(__CLASS__.'::'.$function), $method, 'caller() test reflexion method');
  }
  /**
   * @expectedException OutOfBoundsException
   */
  public function testReflexionFunctionFromStatic() {
    $function = self::callerStandardStaticParam(CALLER_FUNCTION | CALLER_REFLEXION, $line, $funcname);
  }
  public function testReflexionFunctionMethodFromStatic() {
    $array = self::callerStandardStaticParam(CALLER_METHOD | CALLER_FUNCTION | CALLER_REFLEXION, $line, $funcname);
    $this->assertEquals(
      [
        'method' => new ReflectionMethod(__CLASS__.'::'.$funcname)
      ],
      $array
    );
  }
  /**
   * @expectedException OutOfBoundsException
   */
  public function testReflexionClassMethodFromFunction() {
    $array = callerTest_callerReflexionFunction(CALLER_METHOD | CALLER_CLASS | CALLER_REFLEXION);
  }
  public function testReflexionArray() {
    $array = callerTest_callerReflexionFunction(CALLER_ARRAY | CALLER_REFLEXION, $function, $line);
    $this->assertEquals(
      [
        'file' => __FILE__,
        'line' => $line,
        'function' => $function,
        'type' => null,
        'class' => null,
        'object' => null,
      ],
      $array
    );
  }
  public function testReflexionArrayFunction() {
    $array = callerTest_callerReflexionFunction(CALLER_FUNCTION | CALLER_ARRAY | CALLER_REFLEXION, $function, $line);
    $this->assertEquals(
      [
        'raw' => [
          'file' => __FILE__,
          'line' => $line,
          'function' => $function,
          'type' => null,
          'class' => null,
          'object' => null,
        ],
        'function' => new ReflectionFunction($function),
      ],
      $array
    );
  }
}
function callerTest_callerReflexionFunction($param, &$function = null, &$line = null) {
  $function = __FUNCTION__;
  $line = __LINE__ + 1;
  return callerTest::calledStandardParam($param);
}
