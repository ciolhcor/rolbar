<?php


/**
 * @brief Param pour caller()
 */
define('CALLER_OBJECT',        0b00000001);
/**
 * @brief Param pour caller()
 */
define('CALLER_CLASS',         0b00000010);
/**
 * @brief Param pour caller()
 */
define('CALLER_METHOD',        0b00000100);
/**
 * @brief Param pour caller()
 */
define('CALLER_FUNCTION',      0b00001000);
/**
 * @brief Param pour caller()
 */
define('CALLER_ARRAY',         0b00010000);
/**
 * @brief Param pour caller()
 */
define('CALLER_REFLEXION',     0b10000000);


/**
 * @author Ciol Hcor
 * @version 0.0.1
 * @date 2016-11-27
 * @since 0.0.0
 * @brief Retourne les informations de l'appellant de la fonction qui appelle "caller"
 * @details Retourne soit l'objet appelant soit un tableaux des informations.
 * @param $param Indique ce que la fonction doit retourné (voir constante CALLER_*)
 * @retval array S'il y a plusieurs type demandé ou aucun
 * @retval object S'il n'y a qu'un seul type demandé
 */
function caller(int $param = CALLER_ARRAY) {
  $trace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT|DEBUG_BACKTRACE_IGNORE_ARGS, 3);
  $ret = [];
  $ret['file'] = $trace[1]['file'] ?? null;
  $ret['line'] = $trace[1]['line'] ?? null;
  $ret['function'] = $trace[2]['function'] ?? null;
  $ret['type'] = $trace[2]['type'] ?? null;
  $ret['class'] = $trace[2]['class'] ?? null;
  $ret['object'] = $trace[2]['object'] ?? null;
  if($param === CALLER_ARRAY)
    return $ret;
  if($param === CALLER_OBJECT && isset($ret['object']))
    return $ret['object'];
  if($param === CALLER_CLASS && isset($ret['class']))
    return $ret['class'];
  if($param === CALLER_METHOD && isset($ret['class']) && isset($ret['function']))
    return $ret['class'].'::'.$ret['function'];
  if($param === CALLER_FUNCTION && isset($ret['function']))
    return $ret['function'];
  if($param & CALLER_REFLEXION)
  {
    $param ^= CALLER_REFLEXION;
    $array = [];
    if($param & CALLER_OBJECT && isset($ret['object']))
      $array['object'] = new ReflectionObject($ret['object']);
    if($param == CALLER_OBJECT && isset($array['object']))
      return $array['object'];

    if($param & CALLER_CLASS && isset($ret['class']))
      $array['class'] = new ReflectionClass($ret['class']);
    if($param == CALLER_CLASS && isset($array['class']))
      return $array['class'];
    
    if($param & CALLER_METHOD && isset($ret['class']) && isset($ret['function']))
      $array['method'] = new ReflectionMethod($ret['class'].'::'.$ret['function']);
    if($param == CALLER_METHOD && isset($array['method']))
      return $array['method'];

    if($param & CALLER_FUNCTION && isset($ret['function']) && !isset($ret['class']))
      $array['function'] = new ReflectionFunction($ret['function']);
    if($param == CALLER_FUNCTION && isset($array['function']))
      return $array['function'];

    if($param == CALLER_ARRAY)
      return $ret;
    if($param & CALLER_ARRAY)
      $array['raw'] = $ret;

    if(empty($array))
      throw new OutOfBoundsException();
    else
      return $array;
  }

  throw new OutOfBoundsException();
}

