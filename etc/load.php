<?php

if(!defined('MOD')) define('MOD', 'PROD');

define('LIB_DIR', __DIR__.'/../lib/');
define('ETC_DIR', __DIR__.'/../etc/');

// Chargement des fonctions.
foreach(
  new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator(
      LIB_DIR,
      FilesystemIterator::SKIP_DOTS | FilesystemIterator::CURRENT_AS_PATHNAME
    )
  )
  as $i
) 
  if(preg_match('~/[^\.]+\.func\.php$~', $i))
    require_once($i);



// Auto-chargement des classes lors de leur première utilisation.
spl_autoload_register(function ($cls) {
  $fname = LIB_DIR.str_replace('\\', '/', $cls).'.cls.php';
  if(is_file($fname))
    require_once($fname);
});
