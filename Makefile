SQL=$(wildcard install/*.sql)
SQL_TST=$(patsubst install%,install_test%,$(SQL))

all: doc $(SQL_TST) tests

doc:
	@echo "$$ doxygen"
	@doxygen

tests:
	@echo "$$ phpunit"
	@phpunit

export/struct.sql:
	@echo "> $@"
	@mkdir -p export
	@php -r 'require "etc/db.php"; passthru("mysqldump $$DB[dbname] --skip-dump-date -u $$DB[usr] --password=\"$$DB[pwd]\" -h $$DB[host] -P $$DB[port] --no-data");' > $@
	
export/data.sql:
	@echo "> $@"
	@mkdir -p export
	@php -r 'require "etc/db.php"; passthru("mysqldump $$DB[dbname] --skip-dump-date -u $$DB[usr] --password=\"$$DB[pwd]\" -h $$DB[host] -P $$DB[port] --no-create-info");' > $@

export: export/struct.sql export/data.sql

install_test/%.sql: install/%.sql
	@echo "> $@"
	@sed 's/CREATE TABLE/CREATE TEMPORARY TABLE/g' $< | sed 's/DROP TABLE/DROP TEMPORARY TABLE/g' > $@

clean:
	rm -fr doc
mrproper: clean
	rm -fr export
	rm -f install_test/*.sql

ChangeLog:
	@echo "> $@"
	@echo ' Do not modify this file Manualy' > ChangeLog
	@git log --date-order --reverse --format='%ai%x09%an <%ae> %n%x09* %B%n' | sed -e 's/^\([0-9]*-[0-9]*-[0-9]*\) [0-9]*:[0-9]\{2\}:[0-9]\{2\} +[0-9]\+/\1/g' | sed -e 's/^\([^0-9\t]\)/\t* \1/g' | cat -n | sort -uk 2 | sort -g | sed 's/^[^\t]*\t//' | sed '/^$$/d' >> ChangeLog

.PHONY: doc test export export/* clean mrproper ChangeLog



#
#
#
